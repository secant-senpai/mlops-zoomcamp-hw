import os
import mlflow
import pandas as pd
import pickle

from datetime import datetime
from dateutil.relativedelta import relativedelta
from pathlib import Path
from prefect import flow, task, get_run_logger
from prefect.task_runners import SequentialTaskRunner
from sklearn.feature_extraction import DictVectorizer
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
from typing import List

ROOT_DIR = Path(__file__).parent.parent
model_list: List[str] = []

# Initialize MLFlow for Experiment Tracking
MLFLOW_TRACKING_URI = 'sqlite:///mlflow.db'
# MLFLOW_TRACKING_URI = os.getenv('MLFLOW_TRACKING_URI')
EXPERIMENT_NAME = "nyc-fhv-taxi-experiment"
mlflow.set_tracking_uri(MLFLOW_TRACKING_URI)
mlflow.set_experiment(EXPERIMENT_NAME)

@task
def get_paths(date: str=None):
    """
    Derive dataset paths from specified date.
    Train dataset: 2 months before.
    Validation dataset: 1 month before.
    :param date: Date in YYYY-mm-dd format.
    """

    logger = get_run_logger("Get Dataset Paths")

    if date is None:
        date = datetime.now()
        input_date = date.strftime("%Y-%m-%d")
    else:
        input_date = date
        date = datetime.strptime(date, "%Y-%m-%d")

    logger.info("Current date is {}".format(input_date))

    train_date = (date - relativedelta(months=2)).strftime("%Y-%m")
    val_date = (date - relativedelta(months=1)).strftime("%Y-%m")

    logger.info("Checking for existing model pickle file...")

    if (f'model-{input_date}.bin' not in model_list) or (f'dv-{input_date}.b' not in model_list):
        logger.info("...Model pickle file not found, retraining...")
        train_path = '{}/data/fhv_tripdata_{}.parquet'.format(ROOT_DIR, str(train_date))
        val_path ='{}/data/fhv_tripdata_{}.parquet'.format(ROOT_DIR, str(val_date))
    else:
        logger.info("...Model pickle file found! No need to retrain =)...")
        train_path = None       # No training
        val_path ='{}/data/fhv_tripdata_{}.parquet'.format(ROOT_DIR, str(val_date))

    return train_path, val_path


@task
def read_data(path):
    df = pd.read_parquet(path)
    return df


@task
def prepare_features(df, categorical, train=True):

    logger = get_run_logger("Preprocessing")

    df['duration'] = df.dropOff_datetime - df.pickup_datetime
    df['duration'] = df.duration.dt.total_seconds() / 60
    df = df[(df.duration >= 1) & (df.duration <= 60)].copy()

    mean_duration = df.duration.mean()
    if train:
        logger.info(f"The mean duration of training set is {mean_duration}")
    else:
        logger.info(f"The mean duration of validation set is {mean_duration}")
    
    df[categorical] = df[categorical].fillna(-1).astype('int').astype('str')
    return df


@task
def train_model(df, categorical):

    logger = get_run_logger("Training")

    train_dicts = df[categorical].to_dict(orient='records')
    dv = DictVectorizer()
    X_train = dv.fit_transform(train_dicts) 
    y_train = df.duration.values

    logger.info(f"The shape of X_train is {X_train.shape}")
    logger.info(f"The DictVectorizer has {len(dv.feature_names_)} features")

    lr = LinearRegression()
    lr.fit(X_train, y_train)
    y_pred = lr.predict(X_train)
    rmse = mean_squared_error(y_train, y_pred, squared=False)
    mlflow.log_metric("train_rmse", rmse)
    logger.info(f"The MSE of training is: {rmse}")

    return lr, dv


@task
def run_model(df, categorical, dv, lr):

    logger = get_run_logger("Run model")

    val_dicts = df[categorical].to_dict(orient='records')
    X_val = dv.transform(val_dicts) 
    y_pred = lr.predict(X_val)
    y_val = df.duration.values

    rmse = mean_squared_error(y_val, y_pred, squared=False)
    mlflow.log_metric("test_rmse", rmse)
    logger.info(f"The MSE of validation is: {rmse}")
    return


@flow(
    name="training pipeline", 
    task_runner=SequentialTaskRunner()
)
# def main(date="2021-08-15"):
def main(date="2021-03-15"):

    for (roots, dirs, files) in os.walk("./models"):
        for f in files:
            model_list.append(f)

    train_path, val_path = get_paths(date).result()

    categorical = ['PUlocationID', 'DOlocationID']

    if train_path is not None:
        df_train = read_data(train_path)
        df_train_processed = prepare_features(df_train, categorical)

    df_val = read_data(val_path)
    df_val_processed = prepare_features(df_val, categorical, train=False)

    # train the model
    with mlflow.start_run():

        mlflow.set_tag("model_type", "linear_reg")
        mlflow.set_tag("predict_date", date)

        if train_path is not None:
            lr, dv = train_model(df_train_processed, categorical).result()
            mlflow.log_param("train_data", train_path)
            mlflow.log_param("val_data", val_path)

            # Save model and DictVectorizer
            with open(f'models/model-{date}.bin', 'wb') as f_out:
                pickle.dump(lr, f_out)
            with open(f'models/dv-{date}.b', 'wb') as dv_out:
                pickle.dump(dv, dv_out)
            mlflow.log_artifact(local_path=f'models/dv-{date}.b', artifact_path='preprocessor')
            mlflow.sklearn.log_model(lr, artifact_path='model')
        else:
            mlflow.log_param("val_data", val_path)
            # Load model for inference
            lr_file_path = f'models/model-{date}.bin'
            dv_file_path = f'models/dv-{date}.b'
            with open(lr_file_path, 'rb') as f_lr:
                lr = pickle.load(f_lr)
            with open(dv_file_path, 'rb') as dv_lr:
                dv = pickle.load(dv_lr)
        run_model(df_val_processed, categorical, dv, lr)

if __name__ == '__main__':
    main()
