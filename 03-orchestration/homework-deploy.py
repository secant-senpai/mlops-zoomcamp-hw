# Deployment
from prefect.deployments import DeploymentSpec
from prefect.flow_runners import SubprocessFlowRunner
from prefect.orion.schemas.schedules import CronSchedule

DeploymentSpec(
    flow_location='./homework.py',
    flow_name="training pipeline",
    name="model_training",
    # https://crontab.guru/#0_9_15_*_*
    schedule=CronSchedule(cron="0 9 15 * *", timezone="Asia/Kuala_Lumpur"),
    flow_runner=SubprocessFlowRunner(),     # To fetch script from local.
    tags=["train"]
)
