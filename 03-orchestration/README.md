# Week 3 Homework - Workflow Orchestration with Prefect

### Results TLDR
Q1. `train_model` needs `.result()` to work correctly since we want these functions to return two different objects and instead of the PrefectFuture object.  
Q2. Validation RMSE: `11.640 minutes ≈ 11.637 minutes (nearest ans)`  
Q3. DictVectorizer file size: `12880 bytes ≈ 13,000 bytes (nearest ans)`  
Q4. Cron expression to run a flow at 9 AM every 15th of the month:
```
0 9 15 * *
```
Q5. Number of scheduled runs observed in Prefect UI: 3  
Q6. To view all work-queues in the current deployment, one have to run `prefect work-queue ls`.  
`prefect work-queue preview <ID> -h <hours>` is used to preview scheduled runs in the work-queue.


![Model Information in MLFlow UI](resources/MlFlowUI.png "MLFlow UI")  
Figure 1: Model information in MLFlow UI showing train and test data, train and test metrics and date of prediction.  
![dv-2021-08-15 File Size](resources/size_dv.png "dv-2021-08-15")  
Figure 2: File size for DictVectorizer.  
![Number of scheduled runs on Prefect UI](resources/Upcoming_Runs.png "Number of scheduled runs")
Figure 3: Number of scheduled runs for Deployment.
![Preview work queues](resources/ViewWorkQueues.png "Preview work queues using Prefect CLI")
Figure 4: Using Prefect CLI to preview the work queue configured via the UI.

----

### Project Setup

1. Create virtual environment in root directory.  
```
python3.9 -m venv env
source env/bin/activate
```
2. Install dependencies in the created environment.
```
pip install --upgrade pip
pip install -r requirements.txt
```
3. Start MLFlow server in `03-orchestration` folder by running in Terminal:
```
cd 03-orchestration
mlflow ui --backend-store-uri sqlite:///mlflow.db --default-artifact-root ./artifacts
```
4. Configure storage for Prefect Orion server and then start it. Example of storage location: "`03-orchestration/.prefect`".
```
cd 03-orchestration
prefect storage create && prefect storage ls
prefect orion start
```
5. Configure date and then run `homework.py`.
```
python homework.py
```
6. On successful runs, model artifacts will be stored in `03-orchestration/models`.
7. Go to http://127.0.0.1:4200 to access Prefect UI.
8. Go to http://127.0.0.1:5000 to access MLFlow UI.

### Scheduling runs

1. Customize `DeploymentSpec()` in `homework-deploy.py`, do point `flow_location` and `flow_name` to `homework.py` and `main` respectively.
2. Create deployment in Prefect.
```
prefect deployment create homework-deploy.py
```
3. Check http://127.0.0.1:4200 -> Deployments to see if deployments are scheduled correctly.
    - You can delete deployments using `prefect deployment delete <UUID>`.
4. Go to http://127.0.0.1:4200 -> Work Queue on the side panel to configure a Prefect Agent. Note down the UUID.
5. When you're ready to start the runs, run this in Terminal:
```
prefect agent start <UUID>
```

![How to Configure Work Queue in Prefect UI](resources/workQueue.png "Work Queue")  
Figure 5: Configure filters for work queue.