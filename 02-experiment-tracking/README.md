# Week 2 Homework - Experiment Tracking with MLFlow

### Results TLDR

Q1. MLFlow version: 
```
mlflow, version 1.26.0.
```
Q2. 4 files were saved to OUTPUT_FOLDER.  
Q3. MLFlow logged 17 parameters.  
Q4. `default-artifact-root` is needed to configure where artifacts are stored.  
Q5. Best validation RMSE: 6.628 minutes.  
Q6. The test RMSE of the best model is: 6.55 minutes.  

----

### Project Setup

1. Install dependencies.
```
pip install --upgrade pip
pip install -r requirements.txt
```
2. Start MLFlow server in `02-experiment-tracking` folder by running in Terminal:
```
cd 02-experiment-tracking
mlflow ui --backend-store-uri sqlite:///mlflow.db --default-artifact-root ./artifacts
```
3. Open `homework-week2.ipynb`.
4. Point `ROOT_DIR` to this repo's root directory (`mlops-zoomcamp-hw`).
