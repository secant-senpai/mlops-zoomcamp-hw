# Week 1 Homework - Introduction to MLOps

### Results TLDR

Q1. There are 1154112 records.  
Q2. The average duration is 19.167224093791006 minutes.  
Extra. Number of dropped outliers: 44286  
Q3. Fraction of records with missing values for pickup location ID: 0.8352732770722617  
Q4. Dimensionality of feature matrix: 525  
Q5. RMSE on train data: 10.528519425310185 minutes.  
Q6. RMSE on validation data (Feb data): 11.014285828610237 minutes.

----

### Project Setup

1. Install dependencies.
```
pip install --upgrade pip
pip install -r requirements.txt
```
2. Open `homework-week1.ipynb`.
3. Point `ROOT_DIR` to this repo's root directory (`mlops-zoomcamp-hw`).