# Week 4 Homework - Model Deployment using Flask API, Docker

### Results TLDR
Q1. Mean predicted duration for FHV TripData (2021-02) dataset: `16.191691679979066 ≈ 16.19 minutes`.  
Q2. File size of output file is `19MB`.  
Q3. Command to convert notebook to Python script:
```
jupyter nbconvert --to script ./starter.ipynb
```
Q4. First hash for the `Scikit-Learn` dependency:
```
"sha256:08ef968f6b72033c16c479c966bf37ccd49b06ea91b765e1cc27afefe723920b",
```
Q5. Mean predicted duration for March 2021 dataset: `16.298821614015107 ≈ 16.29 minutes`.  
Q6. Mean predicted duration for April 2021 dataset: `9.967573179784523 ≈ 9.96 minutes`.

### Project Setup

1. Install dependencies using `pipenv`.
```
pip install --upgrade pip
pip install pipenv
cp ./04-deployment/Pipfile ./ && cp ./04-deployment/Pipfile.lock ./
pipenv install --deploy
```

2. To get the API up and running, run the following in Terminal:
```
cd 04-deployment
python -m src.api.server
```

3. To run the API within a Docker container, run the following in Terminal instead:
```
sh build.sh
```

4. To test out the API, run `python test/test_server.py` in Terminal OR use the [Postman](https://www.postman.com) app to call the endpoint.  
![Testing the API using Postman](resources/postman_demo.png "Input request and output response from Postman UI")  
