from flask import Flask, request, jsonify
from src.module.model.model import Model

import datetime
import time
import traceback

app = Flask('duration-regressor')
model = Model()


@app.route('/health', methods=['GET'])
def get_status():
    return jsonify(
        {
            "appName": "duration-regressor",
            "version": "1.0.0",
            "ready": True
        }
    )


@app.route('/predict', methods=['POST'])
def make_prediction():
    response = {}

    inputs_dict = request.get_json()
    print("\n##### Request received! #####")

    try:
        start_time = time.time()
        output = model.predict(inputs_dict)
        duration = time.time() - start_time

        response.update(output)
        response["predictDuration"] = duration
        
        print("##### Prediction completed! #####")
    except Exception as e:
        print("##### Prediction failed! #####")
        return error_response(e)

    return response


def error_response(exception_message):

    return jsonify(
        {
            "predictionTime": datetime.datetime.now(),
            "statusCode": 500,
            "message": str(exception_message),
            "traceback": traceback.format_exc()
        }
    )


if __name__ == "__main__":
    print("Starting server on port 9696\n\n")
    app.run(port=9696)
