import datetime
import pickle
import pandas as pd
import src.module.constants as constants


class Model:

    def __init__(self):
        self.vectorizer, self.model = self.load_model()

    def load_model(self):
        with open(constants.model_pickle, 'rb') as f_in:
            dv, lr = pickle.load(f_in)

        return dv, lr

    def preprocess_data(self, year, month):
        df = pd.read_parquet(
            "https://nyc-tlc.s3.amazonaws.com/trip+data/fhv_tripdata_{}-{}.parquet"
            .format(year, month)
        )
        df['duration'] = df.dropOff_datetime - df.pickup_datetime
        df['duration'] = df.duration.dt.total_seconds() / 60
        df = df[(df.duration >= 1) & (df.duration <= 60)].copy()
        df[constants.categorical] = df[constants.categorical].fillna(-1).astype('int').astype('str')

        return df

    def prepare_outputs(self, year, month, y_pred):
        model_outputs = {}

        model_outputs["inputDate"] = f"{year}-{month}"
        model_outputs["validationDataset"] = \
            f"https://nyc-tlc.s3.amazonaws.com/trip+data/fhv_tripdata_{year}-{month}.parquet",
        model_outputs["predictionTime"] = datetime.datetime.now()
        model_outputs["meanPrediction"] = y_pred.mean()
        model_outputs["outputParquet"] = constants.output_parquet.format(year, month)

        return model_outputs

    def save_parquet(self, df, predictions, year, month):
        df_result = {}
        df_result['ride_id'] = f'{int(year):04d}/{int(month):02d}_' + df.index.astype('str')
        df_result['predictions'] = predictions
        df_result = pd.DataFrame(df_result)
        df_result.to_parquet(
            # Output location be any cloud service.
            constants.output_parquet.format(year, month),
            engine='pyarrow',
            compression=None,
            index=False
        )
        print("Full predictions is saved at '{}' !".format(constants.output_parquet.format(year, month)))

    def predict(self, inputs_dict):
        year = inputs_dict["year"]
        month = inputs_dict["month"]

        inputs_df = self.preprocess_data(year, month)

        processed_data = inputs_df[constants.categorical].to_dict(orient='records')
        X_val = self.vectorizer.transform(processed_data)

        y_pred = self.model.predict(X_val)
        print(f"Q5. Mean predicted duration for FHV TripData ({year}-{month}) dataset: {y_pred.mean()} minutes.")
        outputs_dict = self.prepare_outputs(year, month, y_pred)
        self.save_parquet(inputs_df, y_pred, year, month)

        return outputs_dict
