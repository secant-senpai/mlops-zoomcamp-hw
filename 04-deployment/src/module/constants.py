from pathlib import Path

ROOT_DIR = Path(__file__).parent.parent
DATA_DIR = Path(__file__).parent/"data"

categorical = ['PUlocationID', 'DOlocationID']

model_pickle = f"{DATA_DIR}/model.bin"

output_parquet = "./fhv_tripdata_{}-{}_with_ride_id.parquet"
