from setuptools import setup

"""
This file keeps direct dependencies
"""
setup(
    name='duration-regressor',
    python_requires='>=3.9.0'
    install_requires=[
        'scikit-learn==1.1.0',
        'xgboost==1.6.0'
    ]
)