# MLOps Zoomcamp Tutorial Homeworks

This repo serves to store the homework files for the MLOps Zoomcamp course, can be found [here](https://github.com/DataTalksClub/mlops-zoomcamp).

### Tech Stack (WIP)
![The tech stack used in this project thus far](resources/TechStackSoFar.png "Current Tech Stack (WIP)")